use std::env;

// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut seq1 = 0;
    let mut seq2 = 1;

    // 0, 1, 2, 3, 5
    // 0, 1, 1, 2

    for _i in 1..n {
        // let temp = seq2;
        seq2 = seq1 + seq2;
        seq1 = seq2 - seq1;
    }   
    
    return seq2
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let my_int = args[1].parse::<u32>().unwrap();
    println!("{}", fibonacci_number(my_int));
}
