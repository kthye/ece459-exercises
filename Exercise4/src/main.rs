use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();

    let tx1 = mpsc::Sender::clone(&tx);
    
    thread::spawn(move || {
        let vals = vec![
            String::from("Thread 1: Hello,"), 
            String::from("Thread 1: it's me,"),
            String::from("Thread 1: I've been wondering after all these years you'd like to meet,")
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(500));
        }
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("Thread 2: What's poppin,"), 
            String::from("Thread 2: brand new whip just hopped in,"),
            String::from("Thread 2: I got options,")
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    // iterator recieves in an infinite, blocking manner, until tx's are dropped
    // use try_recv() to check in non-blocking way
    // use recv() to wait in a sinular, blocking manner
    for received in rx {
        println!("Got: {}", received);
    }
}
